#!/usr/bin/python
import sys
import os
import subprocess
import time
import re

# Set the appropriate variable names
VirshHost = "jwallace.austin.ibm.com"

prcfMinionName = "jwadmin.austin.ibm.com"
prcfVirshName = "jwprcf"

MCUMinionName = "jwmcu.austin.ibm.com"
MCUVirshName = "jwmcu"

VmgrMinionName = "jwstsc.austin.ibm.com"
VmgrVirshName = "jwmedia"

MeetingMinionName = "jwdb"
MeetingVirshName = "Meeting"

CommMinionName = "jwcomm.austin.ibm.com"
CommVirshName = "jwcomm"

ProxyMinionName = "jwprx.austin.ibm.com"
ProxyVirshName = "jwprx"

def show_help():
    print "Usage: ",
    print sys.argv[0],
    print " startup|shutdown]"

def check_output(*popenargs, **kwargs):
    process = subprocess.Popen(stdout=subprocess.PIPE, *popenargs, **kwargs)
    output, unused_err = process.communicate()
    retcode = process.poll()
    return output

# parse command line args
if len(sys.argv) < 2:
    show_help()
    sys.exit()

action = sys.argv[1]

if action == "shutdown":
	# Shutdown PR/CF
    # Figure out if the host is up
    cmd = ["salt", prcfMinionName, " test.ping"]
    result = check_output(cmd)
    p = re.compile(r'\bnot\b')
    if p.search(result):
        # not up
        pass
    else:
        cmd = ["salt", prcfMinionName, "cmd.run", "/root/.stopprcf"]
        try:
            subprocess.check_call(cmd)
            print "PR/CF Server stopped successfully"
        except:
            print "Failed to stop PR/CF Server"
        cmd = ["salt", prcfMinionName, "cmd.run", "poweroff"]
        try:
            subprocess.check_call(cmd)
            print prcfMinionName + " powered off successfully"
        except:
            print "Failed to power off " + prcfMinionName

    # Power off MCU
    # Figure out if the host is up
    cmd = ["salt", MCUMinionName, " test.ping"]
    result = check_output(cmd)
    p = re.compile(r'\bnot\b')
    if p.search(result):
        # not up
        pass
    else:
        cmd = ["salt", MCUMinionName, "cmd.run", "poweroff"]
        try:
            subprocess.check_call(cmd)
            print MCUMinionName + " powered off successfully"
        except:
            print "Failed to power off " + MCUMinionName

    # Power off Vmgr
    # Figure out if the host is up
    cmd = ["salt", VmgrMinionName, " test.ping"]
    result = check_output(cmd)
    p = re.compile(r'\bnot\b')
    if p.search(result):
        # not up
        pass
    else:
        cmd = ["salt", VmgrMinionName, "cmd.run", "/root/.stopmedia"]
        try:
            subprocess.check_call(cmd)
            print "Vmgr Server stopped successfully"
        except:
            print "Failed to stop Vmgr Server"
        cmd = ["salt", VmgrMinionName, "cmd.run", "poweroff"]
        try:
            subprocess.check_call(cmd)
            print VmgrMinionName + " powered off successfully"
        except:
            print "Failed to power off " + VmgrMinionName
    # Power off Meetings 
    # Figure out if the host is up
    cmd = ["salt", MeetingMinionName, " test.ping"]
    result = check_output(cmd)
    p = re.compile(r'\bnot\b')
    if p.search(result):
        # not up
        pass
    else:
        cmd = ["salt", MeetingMinionName, "system.poweroff"]
        try:
            subprocess.check_call(cmd)
            print MeetingMinionName + " powered off successfully"
        except:
            print "Failed to power off " + MeetingMinionName
    # Power off Community
    # Figure out if the host is up
    cmd = ["salt", CommMinionName, " test.ping"]
    result = check_output(cmd)
    p = re.compile(r'\bnot\b')
    if p.search(result):
        # not up
        pass
    else:
        cmd = ["salt", CommMinionName, "cmd.run", "/root/.stopSTSC"]
        try:
            subprocess.check_call(cmd)
            print "Community Server stopped successfully"
        except:
            print "Failed to stop Community Server"
        cmd = ["salt", CommMinionName, "cmd.run", "poweroff"]
        try:
            subprocess.check_call(cmd)
            print CommMinionName + " powered off successfully"
        except:
            print "Failed to power off " + CommMinionName
    sys.exit()

elif action == "startup":
    hold = 1
    # Starting up Community
    # Figure out if the host is up
    cmd = ["salt", CommMinionName, " test.ping"]
    result = check_output(cmd)
    p = re.compile(r'\bnot\b')
    if p.search(result):
        # it's not up
        cmd = ["salt", VirshHost, "cmd.run",
               "virsh start {0}".format(CommVirshName)]
        try:
            subprocess.check_call(cmd)
        except:
            print "Failed to power on " + CommMinionName
    # give it time to spin up
    time.sleep(20)
    cmd = ["salt", CommMinionName, "cmd.run",
           "/root/.startSTSC"]
    while hold:
        try:
            subprocess.check_call(cmd)
            print "Community Server started successfully"
            hold = 0
        except:
                print "Failed to start Community Server"
                print "Retrying in 5 sec..."
                time.sleep(5)
    hold = 1
    # Starting up Vmgr
    # Figure out if the host is up
    cmd = ["salt", VmgrMinionName, " test.ping"]
    result = check_output(cmd)
    p = re.compile(r'\bnot\b')
    if p.search(result):
        # it's not up
        cmd = ["salt", VirshHost, "cmd.run",
               "virsh start {0}".format(VmgrVirshName)]
        try:
            subprocess.check_call(cmd)
        except:
            print "Failed to power on " + VmgrMinionName
    # give it time to spin up
    time.sleep(15)
    cmd = ["salt", VmgrMinionName, "cmd.run",
           "/root/.startmedia"]
    while hold:
        try:
            subprocess.check_call(cmd)
            print "STVmgrServer started successfully"
            hold = 0
        except:
                print "Failed to start Vmgr Server"
                print "Retrying in 5 sec..."
                time.sleep(5)
    hold = 1

    # Staring up MCU
    # Figure out if the host is up
    cmd = ["salt", MCUMinionName, " test.ping"]
    result = check_output(cmd)
    p = re.compile(r'\bnot\b')
    if p.search(result):
        # it's not up
        cmd = ["salt", VirshHost, "cmd.run",
               "virsh start {0}".format(MCUVirshName)]
        try:
            subprocess.check_call(cmd)
        except:
            print "Failed to power on " + MCUMinionName

    # Starting up PR/CF
    # Figure out if the host is up
    cmd = ["salt", prcfMinionName, " test.ping"]
    result = check_output(cmd)
    p = re.compile(r'\bnot\b')
    if p.search(result):
        # it's not up
        cmd = ["salt", VirshHost, "cmd.run",
               "virsh start {0}".format(prcfVirshName)]
        try:
            subprocess.check_call(cmd)
        except:
            print "Failed to power on " + prcfMinionName
    # give it time to spin up
    time.sleep(20)
    cmd = ["salt", prcfMinionName, "cmd.run",
           "/root/.startprcf"]
    while hold:
        try:
            subprocess.check_call(cmd)
            print "PR/CF Server started successfully"
            hold = 0
        except:
            print "Failed to start PR/CF Server"
            print "Retrying in 5 sec..."
            time.sleep(5)
    sys.exit()
